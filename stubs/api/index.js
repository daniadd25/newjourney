const router = require("express").Router();

const TWO_SECONDS = 2000;

const wait = (time = TWO_SECONDS) => (req, res, next) =>
    setTimeout(next, time);

router.get('/roads',(req,res) => {
    res.send(require('./mocks/roads/roads.json'))
});

router.get('/timetables',(req,res) => {
    res.send(require('./mocks/timetables/timetables.json'))
});

router.get('/hotels',(req,res) => {
    res.send(require('./mocks/hotels/hotels.json'))
});

router.get('/sites',(req,res) => {
    res.send(require('./mocks/sites/sites.json'))
});

router.get('/info',(req,res) => {
    res.send(require('./mocks/info/info.json'))
});

module.exports = router;
