import React, { AllHTMLAttributes } from "react";

import style from "./style.css";

const Timetable = ({ text1, text2, dest1, dest2, timet }) => (
  <div className={style.timetable}>
    {text1} {dest1} {text2} {dest2}
    <p className={style.times}>{timet}</p>
  </div>
);

export default Timetable;
