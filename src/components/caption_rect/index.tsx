import React from "react";
import style from "./style.css";

const CaptRect = ({ caption }) => <p className={style.capt}>{caption}</p>;

export default CaptRect;
