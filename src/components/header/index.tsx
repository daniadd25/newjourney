import React from "react";
import style from "./style.css";
const Header = ({ caption }) => <h1 className={style.wrapper}>{caption}</h1>;
export default Header;
