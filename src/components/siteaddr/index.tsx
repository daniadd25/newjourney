import React from "react";

import style from "./style.css";

export const SiteAddr = ({ text }) => (
  <div className={style.siteaddr}>{text}</div>
);
