import React from "react";

import style from "./style.css";

export const SiteDescr = ({ text }) => (
  <div className={style.sitedescr}>{text}</div>
);
