import React from "react";

const PictSite = ({ res, alt }) => <img src={res} alt={alt} />;
export default PictSite;
