import React from "react";

import style from "./style.css";

export const AdditInfo = ({ text, links }) => (
  <div className={style.addit_info}>
    {text}
    <br />
    {links.map((link) => (
      <>
        <a key={link.text} href={link.href}>
          {link.text}
        </a>
        <br />
      </>
    ))}
  </div>
);
