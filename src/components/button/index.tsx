import React, { AllHTMLAttributes } from "react";

import style from "./style.css";

export function ButtonLink(page: string) {
  document.location.href = page;
}
interface ButtonProps extends AllHTMLAttributes<HTMLButtonElement> {
  onClick?: () => void;
  caption: string;
}

const Button: React.FC<ButtonProps> = ({ caption, onClick }) => {
  //    debugger;
  return (
    <button className={style.blueLight} onClick={onClick}>
      {caption}
    </button>
  );
};

export default Button;
