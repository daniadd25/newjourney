import React from "react";

import style from "./style.css";

const Hotels = ({ dest1, allhotels }) => (
  <div className={style.hotel}>
    Гостиницы {dest1}
    <br />
    {allhotels}
  </div>
);

export default Hotels;
