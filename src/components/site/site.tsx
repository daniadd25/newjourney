import React from "react";

import style from "./style.css";
import { SiteDescr } from "../sitedescription";
import { SiteAddr } from "../siteaddr";

const Site = ({ siteName, siteImage, siteDesc, siteAddress }) => (
  <div className={style.sitepult}>
    <span className={style.siteName}>{siteName}</span>
    <span className={style.pult_item}>
      <span className={style.site}>
        <img src={siteImage} width="300px" />
      </span>
    </span>
    <span className={style.pult_item}>
      <SiteDescr text={siteDesc} />
    </span>
    <span className={style.pult_item}>
      <SiteAddr text={siteAddress} />
    </span>
  </div>
);

export default Site;
