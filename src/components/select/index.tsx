import React, { Component } from "react";
import Select from "react-select";
import style from "./style.css";
const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: "1px dotted pink",
    color: state.isSelected ? "black" : "blue",
    padding: 20,
  }),
  control: () => ({
    // none of react-select's styles are passed to <Control />
    width: 180,
    height: 40,
    background: "#B1DEBD",
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = "opacity 300ms";

    return { ...provided, opacity, transition };
  },
};
export const Select1 = ({ options, value, onChange }) => (
  <Select
    styles={customStyles}
    options={options}
    defaultValue={value}
    onChange={onChange}
  />
);
