import React from "react";
import map1 from "../../images/map";
import style from "./style.css";
const Pict1 = () => (
  <div className={style.pict1}>
    <img src={map1} width="70%" alt="Первая карта" />
  </div>
);
export default Pict1;
