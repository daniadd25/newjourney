import React from "react";
import { act } from "react-dom/test-utils";
import { mount } from "enzyme";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import mockAdapter from "axios-mock-adapter";

import { commonAxios } from "../../utils/axios";

import Info from "../../info";

import { describe, it, expect, beforeEach } from "@jest/globals";

import { store } from "../../__data__ /store";
import info from "../../../stubs/api/mocks/info/info.json";

const multipleRequest = async (mock, responses) => {
  await act(async () => {
    await mock.onAny().reply((config) => {
      const [method, url, params, ...response] = responses.shift();

      if (config.url.includes(url)) {
        return response;
      }
    });
  });
};

describe("Тестирование всего приложения", () => {
  let mockAxios;
  beforeEach(() => {
    mockAxios = new mockAdapter(commonAxios);
  });
  const history = {
    push: jest.fn(),
  };
  it("Тестируем рендер Info", async () => {
    const setRouteLeaveHook =jest.fn();
    const match = {
      params:{
        id:1,
        router:setRouteLeaveHook
      }
    }
    const component = mount(
      <Provider store={store}>
          <Info match={match}/>
      </Provider>
    );

    expect(component).toMatchSnapshot();

    const responses = [["GET", "/info", {}, 200, { ...info }]];
    // // перехватываем запрос
    await multipleRequest(mockAxios, responses);

    component.update();

    expect(component).toMatchSnapshot();
  });
});
