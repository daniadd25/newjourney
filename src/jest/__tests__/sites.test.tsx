import React from "react";
import { act } from "react-dom/test-utils";
import { mount } from "enzyme";
import { Provider } from "react-redux";
import mockAdapter from "axios-mock-adapter";

import { commonAxios } from "../../utils/axios";

import Sites from "../../sites/sites";

import { describe, it, expect, beforeEach } from "@jest/globals";

import { store } from "../../__data__ /store";
import sites from "../../../stubs/api/mocks/sites/sites.json";

const multipleRequest = async (mock, responses) => {
  await act(async () => {
    await mock.onAny().reply((config) => {
      const [method, url, params, ...response] = responses.shift();

      if (config.url.includes(url)) {
        return response;
      }
    });
  });
};

describe("Тестирование всего приложения", () => {
  let mockAxios;
  beforeEach(() => {
    mockAxios = new mockAdapter(commonAxios);
  });

  it("Тестируем рендер Sites", async () => {
    const setRouteLeaveHook =jest.fn();
    const match = {
      params:{
        id:1,
        router:setRouteLeaveHook
      }
    }
    const component = mount(
      <Provider store={store}>
        <Sites match={match}/>
      </Provider>
    );

    expect(component).toMatchSnapshot();

    // проверка валидации на пустые поля
    // component.find('form').simulate('submit')
    // component.update()

    // // пользователь вводит логин и пароль
    // component.find('input#login-input').simulate('change', {
    //     target: {
    //         value: 'test'
    //     }})

    // component.find('input#password-input').simulate('change', {
    //     target: {
    //         value: 'test'
    //     }})
    // component.update()

    // expect(component).toMatchSnapshot()

    // // логинимся с введенными полями
    // component.find('form').simulate('submit')
    // component.update()

    // expect(component).toMatchSnapshot()

    const responses = [["GET", "/sites", {}, 200, { ...sites }]];
    // // перехватываем запрос
    await multipleRequest(mockAxios, responses);

    component.update();

    expect(component).toMatchSnapshot();
  });
});
