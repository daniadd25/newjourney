import React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import Pult from "../../components/pult/pult";

import Map2 from "../../components/map2";
import Pictsite from "../../components/pictsite/pictsite";
import most1 from "../../images/siteimages";

describe("тестируем компоненты", () => {
  it("создается Pult", () => {
    const wrapper = mount(<Pult />);

    expect(wrapper).toMatchSnapshot();
  });
});
describe("тестируем компоненты", () => {
  it("создается карта", () => {
    const wrapper = mount(<Map2 />);

    expect(wrapper).toMatchSnapshot();
  });
});

describe("тестируем компоненты", () => {
  it("создается картинка", () => {
    const wrapper = mount(<Pictsite res={most1} alt={"мост"} />);

    expect(wrapper).toMatchSnapshot();
  });
});
