import React from "react";
import { act } from "react-dom/test-utils";
import { mount } from "enzyme";
import { Provider } from "react-redux";
import mockAdapter from "axios-mock-adapter";

import { commonAxios } from "../../utils/axios";

import Main from "../../main/main";

import { describe, it, expect, beforeEach } from "@jest/globals";

import { store } from "../../__data__ /store";
import timetables from "../../../stubs/api/mocks/timetables/timetables.json";
import hotels from "../../../stubs/api/mocks/hotels/hotels.json";
import roads from "../../../stubs/api/mocks/roads/roads.json";

const multipleRequest = async (mock, responses) => {
  await act(async () => {
    await mock.onAny().reply((config) => {
      const [method, url, params, ...response] = responses.shift();

      if (config.url.includes(url)) {
        return response;
      }
    });
  });
};

describe("Тестирование всего приложения", () => {
  let mockAxios;
  beforeEach(() => {
    mockAxios = new mockAdapter(commonAxios);
  });

  it("Тестируем рендер Main", async () => {
    // expect.assertions(2)
    // первый рендер
    const component = mount(
      <Provider store={store}>
        <Main />
      </Provider>
    );

    expect(component).toMatchSnapshot();

    // проверка валидации на пустые поля
    // component.find('form').simulate('submit')
    // component.update()

    // // пользователь вводит логин и пароль
    // component.find('input#login-input').simulate('change', {
    //     target: {
    //         value: 'test'
    //     }})

    // component.find('input#password-input').simulate('change', {
    //     target: {
    //         value: 'test'
    //     }})
    // component.update()

    // expect(component).toMatchSnapshot()

    // // логинимся с введенными полями
    // component.find('form').simulate('submit')
    // component.update()

    // expect(component).toMatchSnapshot()

    const responses = [
      ["GET", "/timetables", {}, 200, { ...timetables }],
      ["GET", "/hotels", {}, 200, { ...hotels }],
      ["GET", "/roads", {}, 200, { ...roads }],
    ];
    // // перехватываем запрос
    await multipleRequest(mockAxios, responses);

    component.update();

    expect(component).toMatchSnapshot();
  });
});
