import minaret from "./kasimov/minaret.jpg";
import esenin from "./ryazan/esenin.jpg";
import kreml from "./ryazan/kreml.jpg";
import most1 from "./mikhailov/most1.jpg";
import dmitr from "./skopin/dmitr.jpg";

export default { minaret, esenin, kreml, most1, dmitr };
