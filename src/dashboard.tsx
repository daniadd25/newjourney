import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Main from "./main/main";
import Sites from "./sites/sites";
import Info from "./info";
import { URLs } from "./__data__ /urls";
const Dashboard = () => (
  <Switch>
    <Route exact path="/">
      <Main />
    </Route>
    <Route path={URLs.sites.url + "/:id"}>
      <Sites />
    </Route>
    <Route path={URLs.info.url + "/:id"}>
      <Info />
    </Route>
    <Route path="*">
      <Redirect to="/" />
    </Route>
  </Switch>
);

export default Dashboard;
