import React, { useEffect, useState } from "react";
import Button from "../components/button";
import style from "../main/style.css";
import { URLs } from "../__data__ /urls";
import { Link } from "react-router-dom";
import Header from "../components/header";
import Site from "../components/site/site";
import i18next from "i18next";
import siteimages from "../images/siteimages";

import { connect, batch } from "react-redux";
import { getSites } from "../__data__ /actions/sites";
import { RouteComponentProps, withRouter } from "react-router";
import { fromJSON } from "postcss";

type MapStateToProps = {
  sites: any;
  loading: boolean;
};
type MapDispatchToProps = {
  getSites: () => void;
};
type MainProps = MapStateToProps & MapDispatchToProps & RouteComponentProps<{}>;
//const roads = require("../../stubs/api/mocks/roads/roads.json");
//const all_sites=require("../../stubs/api/mocks/sites/sites.json");
const Sites = ({
  sites,
  loading,
  getSites,
  ...routeProps
}: React.PropsWithChildren<MainProps>) => {
  const id = routeProps.match.params.id;
  const [currentSites, setCurrentSites] = useState();
  useEffect(() => {
    batch(() => {
      getSites();
    });
  }, []);
  useEffect(() => {
    if (sites) {
      setCurrentSites(sites.filter((site) => site.city == id));
    }
  }, [id, sites]);
  if (loading || !sites || !currentSites) {
    return <p>loading...</p>;
  }

  return (
    <React.Fragment>
      <Header caption={i18next.t("newjourney.sites.heading")} />
      {currentSites.map((site) => (
        <Site
          siteName={site.name}
          siteImage={siteimages[site.src]}
          siteDesc={site.description}
          siteAddress={site.address}
        />
      ))}

      <div className={style.pult2}>
        <Link to="/">
          <Button caption={i18next.t("newjourney.sites.button_main")} />
        </Link>
        <span className={style.pult_item}>
          <Link to={URLs.info.url + `/${id}`}>
            <Button caption={i18next.t("newjourney.main.button_info")} />
          </Link>
        </span>
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = (state): MapStateToProps => ({
  sites: state.sites.sites,
  loading: state.sites.loading,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
  getSites: () => dispatch(getSites()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sites));
