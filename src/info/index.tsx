import React, { useState, useEffect } from "react";
import Button from "../components/button";
import style from "../main/style.css";
import { URLs } from "../__data__ /urls";
import { Link } from "react-router-dom";
import Header from "../components/header";
import Site from "../components/site/site";
import i18next from "i18next";
import { AdditInfo } from "../components/addit_info";
import Hotels from "../components/hotels";

import { connect, batch } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";

import { getInfo } from "../__data__ /actions/info";

type MapStateToProps = {
  info: any;
  loading: boolean;
};
type MapDispatchToProps = {
  getInfo: () => void;
};
type MainProps = MapStateToProps & MapDispatchToProps & RouteComponentProps<{}>;
const Info = ({
  info,
  loading,
  getInfo,
  ...routeProps
}: React.PropsWithChildren<MainProps>) => {
  console.log({routeProps},routeProps.match,routeProps.match?.params);
  const id = routeProps.match.params.id;
  const [currentInfo, setCurrentInfo] = useState();

  useEffect(() => {
    batch(() => {
      getInfo();
    });
  }, []);
  useEffect(() => {
    if (info) {
      setCurrentInfo(info.find((inf) => inf.id == id));
    }
  }, [id, info]);
  if (loading || !info || !currentInfo) {
    return <p>loading...</p>;
  }

  return (
    <React.Fragment>
      <Header caption={i18next.t("newjourney.info.heading")} />

      <div className={style.pult2}>
        <AdditInfo text={currentInfo.description} links={currentInfo.links} />
      </div>

      <div className={style.pult2}>
        <Link to="/">
          <Button caption={i18next.t("newjourney.sites.button_main")} />
        </Link>
        <span className={style.pult_item}>
          <Link to={URLs.sites.url + `/${id}`}>
            <Button caption={i18next.t("newjourney.main.button_attractions")} />
          </Link>
        </span>
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = (state): MapStateToProps => ({
  info: state.info.info,
  loading: state.info.loading,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
  getInfo: () => dispatch(getInfo()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Info));
