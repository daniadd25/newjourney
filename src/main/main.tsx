import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import Header from "../components/header";
import Pict1 from "../components/pict1/pict1";
import Button, { ButtonLink } from "../components/button";
import CaptRect from "../components/caption_rect";
import Map2 from "../components/map2";
import { Select1 } from "../components/select";
import Timetable from "../components/timetable";
import style from "./style.css";
import Hotels from "../components/hotels";
import Pult1 from "../components/pult/pult";
import { URLs } from "../__data__ /urls";
import value from "*.jpg";
import i18next from "i18next";
import { connect, batch } from "react-redux";
import { getTimetables, getHotels, getRoads } from "../__data__ /actions/main";

type MapStateToProps = {
  timetables: any;
  roads: any;
  hotels: any;
  loading: boolean;
};
type MapDispatchToProps = {
  getTimetables: () => void;
  getHotels: () => void;
  getRoads: () => void;
};
type MainProps = MapStateToProps & MapDispatchToProps;

//const roads = require("../../stubs/api/mocks/roads/roads.json");
//const hotels_list = require("../../stubs/api/mocks/hotels/hotels.json");
//import Pult1 from '../../components/pult';

const Main = ({
  timetables,
  hotels,
  roads,
  loading,
  getTimetables,
  getHotels,
  getRoads,
}: React.PropsWithChildren<MainProps>) => {
  const [selectedCity, setSelected] = useState();
  const [selectedDestCity, setDest] = useState();
  const [selectedHotels, setHotels] = useState("Нет информации о гостиницах");
  useEffect(() => {
    batch(() => {
      getTimetables();
      getHotels();
      getRoads();
    });


  }, []);
  useEffect(() => {
    if (roads) {
      setSelected(roads[0]);
      setDest(roads[0].dest[0]);
    }
  }, [roads]);

  if (loading || !roads || !timetables || !hotels || !selectedCity) {
    return <p>loading...</p>;
  }
  function getDestCities(city) {
    return city.dest.map((cityId) => roads.find((city) => city.id == cityId));
  }

  function handleSelect1Change(selectedOption) {
    const newSelectedCity = roads.find(
      (city) => city.id == selectedOption.value
    );
    setSelected(newSelectedCity);
    setDest(newSelectedCity.dest[0]);
    setHotels("Нет информации о гостиницах");
  }
  function handleSelectDestChange(selectedOption) {
    setDest(
      getDestCities(selectedCity).find(
        (city) => city.id == selectedOption.value
      )
    );
  }
  function getTimetable(dep, dest) {
    if (timetables.find((city) => city.dep == dep && city.to == dest)) {
      return timetables
        .find((city) => city.dep == dep && city.to == dest)
        .time.join(", ");
    }
    return <p>Расписание недоступно</p>;
  }

  function getCityHotels() {
    let allh = "Нет информации о гостиницах";
    let dest_hotels = [];
    for (let city of hotels) {
      if (city.dest == selectedCity.id) {
        dest_hotels.push(city);
      }
    }
    if (dest_hotels.length > 0) {
      allh = "";
      for (let hotel of dest_hotels) {
        allh +=
          hotel.hotel +
          " " +
          hotel.phone +
          " " +
          hotel.address +
          " цена от " +
          hotel.min_price +
          "\n\n";
      }
    }
    setHotels(allh);
  }

  return (
    <React.Fragment>
      <Header caption={i18next.t("newjourney.main.heading")} />
      <Pict1 />
      <div className={style.pult1}>
        <CaptRect caption={i18next.t("newjourney.main.caption1")} />

        <span className={style.pult_item}>
          <Select1
            options={roads.map((city) => ({
              value: city.id,
              label: city.name,
            }))}
            value={{ value: selectedCity.id, label: selectedCity.name }}
            onChange={handleSelect1Change}
          />
        </span>
        <span className={style.pult_item}>
          <Button
            caption={i18next.t("newjourney.main.button_hotels")}
            onClick={getCityHotels}
          />
        </span>
        <span className={style.pult_item}>
          <Link to={URLs.sites.url + `/${selectedCity.id}`}>
            <Button caption={i18next.t("newjourney.main.button_attractions")} />
          </Link>
        </span>
        <span className={style.pult_item}>
          <Link to={URLs.info.url + `/${selectedCity.id}`}>
            <Button caption={i18next.t("newjourney.main.button_info")} />
          </Link>
        </span>
      </div>

      <div className={style.pult1}>
        <CaptRect caption={i18next.t("newjourney.main.caption2")} />

        <span className={style.pult_item} />
        <Select1
          options={getDestCities(selectedCity).map((city) => ({
            value: city.id,
            label: city.name,
          }))}
          value={{ value: selectedDestCity.id, label: selectedDestCity.name }}
          onChange={handleSelectDestChange}
        />
      </div>
      <div className={style.pult1}>
        <Timetable
          dest1={selectedCity.name}
          dest2={selectedDestCity.name}
          text1={i18next.t("newjourney.main.timetable.text1")}
          text2={i18next.t("newjourney.main.timetable.text2")}
          timet={getTimetable(selectedCity.id, selectedDestCity.id)}
        />

        <span className={style.pult_item} />

        <Hotels dest1={selectedCity.name} allhotels={selectedHotels} />
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = (state): MapStateToProps => ({
  timetables: state.main.timetables,
  roads: state.main.roads,
  hotels: state.main.hotels,
  loading: state.main.loading,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
  getTimetables: () => dispatch(getTimetables()),
  getHotels: () => dispatch(getHotels()),
  getRoads: () => dispatch(getRoads()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
