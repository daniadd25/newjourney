import React from "react";
import { BrowserRouter } from "react-router-dom";
import Dashboard from "./dashboard";
import Main from "./main/main";
import { Provider } from "react-redux";
import { store } from "./__data__ /store";
//import Sites from './sites/sites';
const App = () => (
  <Provider store={store}>
    <BrowserRouter basename="/newjourney">
      <Dashboard />
    </BrowserRouter>
  </Provider>
);

export default App;
