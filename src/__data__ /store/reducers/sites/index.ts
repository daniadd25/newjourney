import * as types from "../../../action-types";

export type SitesState = {
  sites: any;
  loading: boolean;
  error: boolean;
};

const initialState: SitesState = {
  loading: false,
  error: false,
  sites: null,
};

const handleRequest = (state, action) => ({
  ...state,
  loading: true,
  error: false,
});
const handleSuccess = (state, action) => ({
  ...state,
  loading: false,
  sites: action.payload,
});

const handleFailure = (state, action) => ({
  ...state,
  loading: false,
  error: action.payload,
});

export default function (state = initialState, action) {
  switch (action.type) {
    case types.SITES.REQUEST:
      return handleRequest(state, action);
    case types.SITES.SUCCESS:
      return handleSuccess(state, action);
    case types.SITES.FAILURE:
      return handleFailure(state, action);
    default:
      return state;
  }
}
