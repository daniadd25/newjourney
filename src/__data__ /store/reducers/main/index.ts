import * as types from "../../../action-types";

export type MainState = {
  timetables: any;
  roads: any;
  hotels: any;
  loading: boolean;
  error: boolean;
};

const initialState: MainState = {
  loading: false,
  error: false,
  timetables: null,
  roads: null,
  hotels: null,
};

const handleRequest = (state, action) => ({
  ...state,
  loading: true,
  error: false,
});
const handleTimetablesSuccess = (state, action) => ({
  ...state,
  loading: false,
  timetables: action.payload,
});
const handleRoadsSuccess = (state, action) => ({
  ...state,
  loading: false,
  roads: action.payload,
});
const handleHotelsSuccess = (state, action) => ({
  ...state,
  loading: false,
  hotels: action.payload,
});
const handleFailure = (state, action) => ({
  ...state,
  loading: false,
  error: action.payload,
});

export default function (state = initialState, action) {
  switch (action.type) {
    case types.MAIN.REQUEST:
      return handleRequest(state, action);
    case types.MAIN.TIMETABLES_SUCCESS:
      return handleTimetablesSuccess(state, action);
    case types.MAIN.HOTELS_SUCCESS:
      return handleHotelsSuccess(state, action);
    case types.MAIN.ROADS_SUCCESS:
      return handleRoadsSuccess(state, action);
    case types.MAIN.FAILURE:
      return handleFailure(state, action);
    default:
      return state;
  }
}
