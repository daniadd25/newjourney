import * as types from "../../../action-types";

export type InfoState = {
  info: any;
  loading: boolean;
  error: boolean;
};

const initialState: InfoState = {
  loading: false,
  error: false,
  info: null,
};

const handleRequest = (state, action) => ({
  ...state,
  loading: true,
  error: false,
});
const handleSuccess = (state, action) => ({
  ...state,
  loading: false,
  info: action.payload,
});

const handleFailure = (state, action) => ({
  ...state,
  loading: false,
  error: action.payload,
});

export default function (state = initialState, action) {
  switch (action.type) {
    case types.INFO.REQUEST:
      return handleRequest(state, action);
    case types.INFO.SUCCESS:
      return handleSuccess(state, action);
    case types.INFO.FAILURE:
      return handleFailure(state, action);
    default:
      return state;
  }
}
