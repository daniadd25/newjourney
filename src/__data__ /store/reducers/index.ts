import { combineReducers } from "redux";

import mainReducer, { MainState } from "./main";
import sitesReducer, { SitesState } from "./sites";
import infoReducer, { InfoState } from "./info";

export type AppStore = {
  main: MainState;
  sites: SitesState;
  info: InfoState;
};

export default combineReducers<AppStore>({
  main: mainReducer,
  sites: sitesReducer,
  info: infoReducer,
});
