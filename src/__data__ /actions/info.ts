import { commonAxios } from "../../utils/axios";
import { getConfigValue } from "@ijl/cli";

import * as types from "../action-types";

const requestActionCreator = () => ({ type: types.INFO.REQUEST });
const successActionCreator = (data, type) => ({
  type,
  payload: data,
});
const errorActionCtreator = (error) => ({
  type: types.INFO.FAILURE,
  payload: error,
});

export const getInfo = () => async (dispatch) => {
  const baseApiUrl = getConfigValue("newjourney.api");

  dispatch(requestActionCreator());
  try {
    const response = await commonAxios.get(`/info`);
    dispatch(successActionCreator(response.data.info, types.INFO.SUCCESS));
  } catch (error) {
    dispatch(
      errorActionCtreator(
        error?.response?.data?.message || "Неизвестная ошибка"
      )
    );
  }
};
