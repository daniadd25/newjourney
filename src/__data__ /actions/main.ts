import { commonAxios } from "../../utils/axios";
import { getConfigValue } from "@ijl/cli";

import * as types from "../action-types";

const requestActionCreator = () => ({ type: types.MAIN.REQUEST });
const successActionCreator = (data, type) => ({
  type,
  payload: data,
});
const errorActionCtreator = (error) => ({
  type: types.MAIN.FAILURE,
  payload: error,
});

export const getTimetables = () => async (dispatch) => {
  const baseApiUrl = getConfigValue("newjourney.api");

  dispatch(requestActionCreator());
  try {
    const response = await commonAxios.get(`/timetables`);
    dispatch(
      successActionCreator(
        response.data.timetables,
        types.MAIN.TIMETABLES_SUCCESS
      )
    );
  } catch (error) {
    dispatch(
      errorActionCtreator(
        error?.response?.data?.message || "Неизвестная ошибка"
      )
    );
  }
};

export const getHotels = () => async (dispatch) => {
  const baseApiUrl = getConfigValue("newjourney.api");

  dispatch(requestActionCreator());
  try {
    const response = await commonAxios.get(`/hotels`);
    dispatch(
      successActionCreator(response.data.hotels, types.MAIN.HOTELS_SUCCESS)
    );
  } catch (error) {
    dispatch(
      errorActionCtreator(
        error?.response?.data?.message || "Неизвестная ошибка"
      )
    );
  }
};

export const getRoads = () => async (dispatch) => {
  const baseApiUrl = getConfigValue("newjourney.api");

  dispatch(requestActionCreator());
  try {
    const response = await commonAxios.get(`/roads`);
    dispatch(
      successActionCreator(response.data.roads, types.MAIN.ROADS_SUCCESS)
    );
  } catch (error) {
    dispatch(
      errorActionCtreator(
        error?.response?.data?.message || "Неизвестная ошибка"
      )
    );
  }
};
