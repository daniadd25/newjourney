import { commonAxios } from "../../utils/axios";
import { getConfigValue } from "@ijl/cli";

import * as types from "../action-types";

const requestActionCreator = () => ({ type: types.SITES.REQUEST });
const successActionCreator = (data, type) => ({
  type,
  payload: data,
});
const errorActionCtreator = (error) => ({
  type: types.SITES.FAILURE,
  payload: error,
});

export const getSites = () => async (dispatch) => {
  const baseApiUrl = getConfigValue("newjourney.api");

  dispatch(requestActionCreator());
  try {
    const response = await commonAxios.get(`/sites`);
    dispatch(successActionCreator(response.data.sites, types.SITES.SUCCESS));
  } catch (error) {
    dispatch(
      errorActionCtreator(
        error?.response?.data?.message || "Неизвестная ошибка"
      )
    );
  }
};
