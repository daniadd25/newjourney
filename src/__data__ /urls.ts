import React from "react";
import { getNavigations } from "@ijl/cli";

const navigations = getNavigations("newjourney");

export const baseUrl = navigations["newjourney"];

export const URLs = {
  sites: { url: navigations["link.newjourney.sites"] },
  info: { url: navigations["link.newjourney.info"] },
};
